# Discord ad blocker
A Discord client theme that attempts to block as many of Discord's Nitro ads, upsells and pop-ups as possible.

Due to the nature of Discord's feature/upsell rollouts, I may not be able to guarantee that all new ads are blocked as soon as they're added - I will attempt to keep this repo as up-to-date as possible, but I can only block ads that I am aware of (i.e. as I encounter them in my own client.)

If you do manage to find something I've missed, I would appreciate that you submit an issue with details (ideally including the object class(es) of the ad in question.)

## This theme does:
- Attempt to hide all Discord Nitro "advertisements" (annoying pop-ups, flashing buttons and watermarks that tell you to try a Nitro feature or bug you to subscribe to Nitro)
- Hide the main Nitro (and Nitro-related) pages

## This theme does NOT:
- Remove functionality from the client that is Nitro-related, such as picking/viewing client themes or sending custom emoji
- Prohibit you from adding/managing/cancelling a Nitro subscription
- Send or receive any additional information to/from Discord (it's as safe to use this theme as any other)